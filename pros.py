
from posixpath import dirname
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import os , sys , shutil
from pathlib import Path
from PIL import Image
from styleing import StyleingUi
from login import *

class ProseccingImageTest(QWidget , StyleingUi):
    def __init__(self):
        self.dict_pixels = dict()
        super(ProseccingImageTest , self).__init__()

        self.button_folder.clicked.connect(self.open_dierctory)
        self.button_image.clicked.connect(self.show_info)
        

    def open_dierctory(self):
        try :
            self.directory = QFileDialog.getExistingDirectory(self,"Choose Directory",".")
            if not Path(self.directory).is_dir() == True :
                self.errr_directory = QMessageBox.critical(self,'Error','Error Directory',QMessageBox.Ok)
            self.list_image = list(filter(lambda x:x.endswith(('.png','.jpg','.jpeg')) , os.listdir(self.directory)))
                
            self.line_path.setText(self.directory)
            self.show_resulation()
        except Exception :
             QMessageBox.critical(self,'Error','Error Directory',QMessageBox.Ok)



    def show_resulation(self):
        try :
            for (self.target_image) in self.list_image :
                self.image = Image.open(self.directory + '\\' + self.target_image)
                self.width , self.height = self.image.size

                self.dict_pixels[(self.target_image)] = sum([self.width , self.height])
            #print(self.dict_pixels)

        except Exception :
             QMessageBox.critical(self,'Error','Invalid Please Try Again!!',QMessageBox.Ok)


    def show_picture(self):
        try:
            self.info_image = list()
            self.name , self.resulation = '',0
            for(self.name_image , self.resulation_image) in self.dict_pixels.items():
                if (self.resulation < self.resulation_image):
                    self.name , self.resulation = (self.name_image , self.resulation_image)

            for check in self.list_image :
                if not check.endswith(('.png','.jpg','.jpeg')) and len(self.list_image) <=0:
                    self.messageError = QMessageBox.critical(self,'Error','Not Image',QMessageBox.Ok)


            self.info_image.extend([self.directory , self.name]) ; return self.info_image


        except Exception :
            QMessageBox.critical(self,'Error','Image Error Please Try Again',QMessageBox.Ok)



    def show_info(self):

        try :
            self.path , self.name = self.show_picture()
            self.information = Image.open(self.path + '/' + self.name)


            self.label_name_image.setText(self.name)       
            self.label_path_image.setText(str(self.path))
            
            self.max_width , self.max_hieght = self.information.size
            self.label_size_image.setText(str(self.max_width) + 'x' + str(self.max_hieght))

            self.label_max_image.setText(str(self.max_width + self.max_hieght))


        except Exception :
            QMessageBox.critical(self,'Error','Image Error Please Try Again',QMessageBox.Ok)
