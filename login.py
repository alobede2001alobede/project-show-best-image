

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from pros import *
import sys

class Login(QWidget):
	def __init__(self):
		self.window_main = None

		super(Login , self).__init__()
		self.setFixedWidth(500)
		self.setFixedHeight(300)
		self.setStyleSheet('background-color:#232b2b;')
		self.setWindowTitle('Sign In')
		self.setWindowIcon(QIcon('sign.png'))


		self.frames()
		self.labels()
		
		self.button_sign = QPushButton(self,text='Sign In')
		self.button_sign.move(170,230)

		self.button_sign.resize(140,30)
		self.button_sign.setStyleSheet(
			'''
			font: 75 13pt "Myanmar Text";
            background-color:#a3c1ad;
			'''
			)

		self.button_sign.clicked.connect(self.get_info_sign)





	def frames(self):
		self.frame1 = QFrame(self)
		self.frame1.setFrameShadow(QFrame.WinPanel | QFrame.Raised)
		self.frame1.setLineWidth(3)
		self.frame1.move(5,14)
		self.frame1.resize(487,280)


	def labels(self):
		self.user_label = QLabel(self,text='User Name')
		self.user_label.move(40,40)
		self.user_label.setStyleSheet(
				'''
                font: 75 15pt "Myanmar Text";
                color:white;
           
                '''
			)

		self.password_label = QLabel(self,text='Password')
		self.password_label.move(40,150)
		self.password_label.setStyleSheet(
				'''
                font: 75 15pt "Myanmar Text";
                color:white;
           
                '''
				)

		self.user_line = QLineEdit(self)
		self.user_line.move(160,44)
		self.user_line.resize(240,28)

		self.user_line.setStyleSheet(
				'''
                font: 75 13pt "Myanmar Text";
                color:white;
           
                '''

				)

		self.pass_line = QLineEdit(self)
		self.pass_line.move(160,149)
		self.pass_line.resize(240,28)

		self.pass_line.setStyleSheet(
				'''
                font: 75 13pt "Myanmar Text";
                color:white;
           
                '''

				)


	def get_info_sign(self):
		if self.user_line.text() == 'Admin' or self.user_line.text() == 'admin':
			if self.pass_line.text() == 'Admin' or self.pass_line.text() == 'admin':
				self.close()
				self.window_main = ProseccingImageTest()
				self.window_main.show()
			else :
				QMessageBox.critical(self,'Invalid ','Invalid Sign In',QMessageBox.Ok)

		else :
			QMessageBox.critical(self,'Invalid ','Invalid Sign In',QMessageBox.Ok)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = Login()

    window.show()
    sys.exit(app.exec_())


