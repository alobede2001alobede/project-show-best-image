

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *


class StyleingUi(object):
    def __init__(self):
        super(StyleingUi , self).__init__()
        self.setWindowTitle('Best Image')
        self.setWindowIcon(QIcon('title.png'))
        self.setFixedHeight(640)
        self.setFixedWidth(1000)
        self.setStyleSheet('background-color:#232b2b;')

        self.all_frame()
        self.all_lines()
        self.all_buttons()
        self.all_labels()

    
    def all_frame(self):
        self.frame1 = QFrame(self)
        self.frame1.setFrameShadow(QFrame.WinPanel | QFrame.Raised)
        self.frame1.setLineWidth(3)
        self.frame1.move(10,20)
        self.frame1.resize(980,161)


        self.frame2 = QFrame(self)
        self.frame2.setFrameShadow(QFrame.Panel | QFrame.Raised)
        self.frame2.setLineWidth(4)
    
        self.frame2.move(10,190)
        self.frame2.resize(980,430)




    def all_lines(self):
        self.line_path = QLineEdit(self)
        self.line_path.move(200,60) ; self.line_path.resize(560,41)
        self.line_path.setPlaceholderText('Please choose a correct image folder')

        self.line_path.setStyleSheet(
                '''
                font-size:17px;
                color:white;
                '''
                )


    def all_buttons(self):
        self.button_image = QPushButton(self,text='Show Info')
        self.button_image.setStyleSheet(
                '''
                font: 75 15pt "Myanmar Text";
                background-color:#c4aead;
                '''
                )

    

        self.button_image.move(40,60)
        self.button_image.resize(130,41)




        self.button_folder = QPushButton(self,text='Open Folder')
        self.button_folder.setStyleSheet(
                '''
                font: 75 15pt "Myanmar Text";
                background-color:#a3c1ad;
                '''
                )
        self.button_folder.move(800,60)
        self.button_folder.resize(130,41)




    def all_labels(self):
        self.n = QLabel(self,text='Name Image :')
        self.n.move(41,230)


        self.n.setStyleSheet(

                '''
                font: 75 16pt "Myanmar Text";
                color:white;
                '''
                )


        self.p = QLabel(self,text='Path Image :')
        self.p.move(41,336)


        self.p.setStyleSheet(

                '''
                font: 75 16pt "Myanmar Text";
                color:white;
                '''
                )

        self.s = QLabel(self,text='Size Image :')
        self.s.move(41,440)


        self.s.setStyleSheet(

                '''
                font: 75 16pt "Myanmar Text";
                color:white;
                '''
                )


        self.m = QLabel(self,text='Sum pixels  :')
        self.m.move(41,530)


        self.m.setStyleSheet(

                '''
                font: 75 16pt "Myanmar Text";
                color:white;
                '''
                )
        ''' The End All Labels Title '''


        self.label_name_image = QLabel(self,text='')
        self.label_path_image = QLabel(self,text='')

        self.label_size_image = QLabel(self,text='')
        self.label_max_image = QLabel(self,text='')



        self.label_name_image.move(240,233)
        self.label_name_image.resize(181,30)





        self.label_name_image.setStyleSheet(
                '''
                font: 75 16pt "Myanmar Text";
                color:#ffbdde;
                '''
                )


        self.label_path_image.move(230,340)
        self.label_path_image.resize(400,30)



        self.label_path_image.setStyleSheet(
                '''
                font: 75 16pt "Myanmar Text";
                color:#ffbdde;
                '''
                )


        self.label_size_image.move(230,440)
        self.label_size_image.resize(400,30)



        self.label_size_image.setStyleSheet(
                '''
                font: 75 16pt "Myanmar Text";
                color:#ffbdde;
                '''
                )


        self.label_max_image.move(230,530)
        self.label_max_image.resize(400,30)

        self.label_max_image.setStyleSheet(
                '''
                font: 75 16pt "Myanmar Text";
                color:#ffbdde;
                '''
                )


    


        